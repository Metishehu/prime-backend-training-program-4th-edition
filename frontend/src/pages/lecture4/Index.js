/**
 * Created by Agon Lohaj on 09/04/2019.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import { PAGES } from 'Constants';
import Intro from "pages/lecture4/Intro";
import HttpRouting from "pages/lecture4/HttpRouting";
import ManipulatingResponses from "pages/lecture4/ManipulatingResponses";
import BodyParsers from "pages/lecture4/BodyParsers";
import HandlingServingJson from "pages/lecture4/HandlingServingJson";
import Assignments from "pages/lecture4/Assignments";
import React from "react";

const styles = () => ({
  root: {},
})

class Index extends React.Component {
  render() {
    const { breadcrumbs } = this.props

    let section = breadcrumbs[0]
    if (breadcrumbs.length > 1) {
      section = breadcrumbs[1]
    }

    const props = {
      section
    }

    switch (section.id) {
      case PAGES.LECTURE_4.PLAY_ROUTING:
        return <HttpRouting {...props} />
      case PAGES.LECTURE_4.MANIPULATING_RESPONSES:
        return <ManipulatingResponses {...props} />
      case PAGES.LECTURE_4.BODY_PARSERS:
        return <BodyParsers {...props} />
      case PAGES.LECTURE_4.HANDLING_SERVING_JSON:
        return <HandlingServingJson {...props} />
      case PAGES.LECTURE_4.ASSIGNMENTS:
        return <Assignments {...props} />
    }
    return <Intro {...props} />
  }
}

export default withStyles(styles)(Index)

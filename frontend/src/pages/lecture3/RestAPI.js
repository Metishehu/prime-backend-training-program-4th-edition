/**
 * Created by LeutrimNeziri on 09/04/2019.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import SimpleLink from "presentations/rows/SimpleLink";
import { Bold } from "presentations/Label";

const styles = ({ typography }) => ({
  root: {},
})

class Intro extends React.Component {
  render() {
    const { classes, section } = this.props
    return (
      <Fragment>
        <Typography variant={'heading'}>
          Section 3: Rest API
          <Typography>
            Resources:
            <ol>
              <li><SimpleLink href="https://restfulapi.net/">https://restfulapi.net/</SimpleLink></li>
            </ol>
          </Typography>
          <Divider />
        </Typography>
        <Typography>
          REST is acronym for REpresentational State Transfer. It is architectural style for distributed hypermedia systems and was first presented by Roy Fielding in 2000 in his famous dissertation.
        </Typography>
        <Typography>
          Guiding Principles of REST
          <ol>
            <li><Bold>Client–server</Bold> – By separating the user interface concerns from the data storage concerns, we improve the portability of the user interface across multiple platforms and improve scalability by simplifying the server components.</li>
            <li><Bold>Stateless</Bold> – Each request from client to server must contain all of the information necessary to understand the request, and cannot take advantage of any stored context on the server. Session state is therefore kept entirely on the client.</li>
            <li><Bold>Cacheable</Bold> – Cache constraints require that the data within a response to a request be implicitly or explicitly labeled as cacheable or non-cacheable. If a response is cacheable, then a client cache is given the right to reuse that response data for later, equivalent requests.</li>
            <li><Bold>Uniform interface</Bold> – By applying the software engineering principle of generality to the component interface, the overall system architecture is simplified and the visibility of interactions is improved. In order to obtain a uniform interface, multiple architectural constraints are needed to guide the behavior of components. REST is defined by four interface constraints: identification of resources; manipulation of resources through representations; self-descriptive messages; and, hypermedia as the engine of application state.</li>
            <li><Bold>Layered system</Bold> – The layered system style allows an architecture to be composed of hierarchical layers by constraining component behavior such that each component cannot “see” beyond the immediate layer with which they are interacting.</li>
            <li><Bold>Code on demand</Bold> (optional) – REST allows client functionality to be extended by downloading and executing code in the form of applets or scripts. This simplifies clients by reducing the number of features required to be pre-implemented.</li>
          </ol>
        </Typography>
        <Typography>
          The key abstraction of information in REST is a resource. Any information that can be named can be a resource: a document or image, a temporal service, a collection of other resources, a non-virtual object (e.g. a person), and so on. REST uses a resource identifier to identify the particular resource involved in an interaction between components.
        </Typography>
        <Typography>
          The state of the resource at any particular timestamp is known as resource representation. A representation consists of data, metadata describing the data and hypermedia links which can help the clients in transition to the next desired state.
        </Typography>

        <Typography>
          The data format of a representation is known as a media type. The media type identifies a specification that defines how a representation is to be processed. A truly RESTful API looks like hypertext. Every addressable unit of information carries an address, either explicitly (e.g., link and id attributes) or implicitly (e.g., derived from the media type definition and representation structure).
        </Typography>
        <Typography>
          A RESTfull API is an api that adhere to the REST architectural constraints. HTTP-based RESTful APIs are defined with the following aspects:
          <ol>
            <li>A base URI, such as http://api.example.com/resource/</li>
            <li>Standard HTTP methods (e.g., GET, POST, PUT, PATCH and DELETE)</li>
          </ol>
          Relationship between URI and HTTP methods can be explained like this:
          <ol>
            <li>GET: Retrieve the URIs of the member resources of the collection resource in the response body. I.E. My songs list!</li>
            <li>POST: Create a member resource in the collection resource using the instructions in the request body. The URI of the created member resource is automatically assigned and returned in the response Location header field! I.E. Add a song to my list!</li>
            <li>PUT: Replace all the representations of the member resources of the collection resource with the representation in the request body, or create the collection resource if it does not exist! Change the song title to something else, or create it if it doesn't exist</li>
            <li>PATCH: Update all the representations of the member resources of the collection resource using the instructions in the request body, or may create the collection resource if it does not exist! Initialise or put a song into my list!</li>
            <li>DELETE: Delete all the representations of the member resources of the collection resource. Well, I don't like this song anymore, remove it!</li>
          </ol>
        </Typography>
        <Typography>
          Every HTTP request will have to respond with one of the following status codes, inspired by <SimpleLink href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Status">HTTP status Codes</SimpleLink>:
          <ol>
            <li>1**: Information Responses</li>
            <li>2**: Successful responses</li>
            <li>3**: Redirection messages</li>
            <li>4**: Client error responses</li>
            <li>5**: Server error responses</li>
          </ol>
          Or in a nutshell:
          <ol>
            <li>1**: Hold on!</li>
            <li>2**: Here you go!</li>
            <li>3**: Go away!</li>
            <li>4**: You fucked up!</li>
            <li>5**: I fucked up!</li>
          </ol>
        </Typography>
      </Fragment>
    )
  }
}

export default withStyles(styles)(Intro)

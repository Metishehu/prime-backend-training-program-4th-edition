/**
 * Created by Agon Lohaj on 27/08/2020.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import SimpleLink from "presentations/rows/SimpleLink";
import { Bold, Highlighted, Italic } from "presentations/Label";
import ActorGraph from 'assets/images/lecture7/actor_graph.png'
import TimelineInvariants from 'assets/images/lecture7/serialized_timeline_invariants.png'

const styles = ({ size, typography }) => ({
  root: {
  },
  img: {
    boxShadow: 'none'
  }
})

const AkkaIntro = (props) => {
  const { classes, section } = props
  const introduction = section.children[0]
  const motivation = section.children[1]
  const actorModel = section.children[2]
  const toolkit = section.children[3]
  // const coordinatedShutDown = section.children[4]
  // const updatingAkkaVersion = section.children[5]

  return (
    <Fragment>
      <Typography variant={'heading'}>
        Section 7: {section.display}
        <Typography>
          Resources: <ol>
          <li><SimpleLink href="https://akka.io/">https://akka.io/</SimpleLink></li>
          <li><SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/typed/guide/introduction.html">https://doc.akka.io/docs/akka/2.6.0/typed/guide/introduction.html</SimpleLink></li>
          <li><SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/typed/guide/actors-motivation.html">https://doc.akka.io/docs/akka/2.6.0/typed/guide/actors-motivation.html</SimpleLink></li>
          <li><SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/typed/guide/actors-intro.html">https://doc.akka.io/docs/akka/2.6.0/typed/guide/actors-intro.html</SimpleLink></li>
          <li><SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/typed/guide/modules.html">https://doc.akka.io/docs/akka/2.6.0/typed/guide/modules.html</SimpleLink></li>
          <li><SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/typed/guide/index.html">https://doc.akka.io/docs/akka/2.6.0/typed/guide/index.html</SimpleLink></li>
        </ol>
          Build powerful reactive, concurrent, and distributed applications more easily
        </Typography>
        <Divider />
      </Typography>
      <Typography>
        <Highlighted>We are going to use version 2.6.0 of Akka, check for compatibility and versioning!</Highlighted>
      </Typography>
      <Typography id={introduction.id} variant={'title'}>
        {introduction.display}
      </Typography>
      <Typography>
        <SimpleLink href="https://akka.io/">Akka</SimpleLink> is a toolkit for building highly concurrent, distributed, and resilient message-driven applications for Java and Scala. Akka is a set of open-source libraries for designing scalable, resilient systems that span processor cores and networks. Akka allows you to focus on meeting business needs instead of writing low-level code to provide reliable behavior, fault tolerance, and high performance.
      </Typography>
      <Typography>
        Many common practices and accepted programming models do not address important challenges inherent in designing systems for modern computer architectures. To be successful, distributed systems must cope in an environment where components crash without responding, messages get lost without a trace on the wire, and network latency fluctuates. These problems occur regularly in carefully managed intra-datacenter environments - even more so in virtualized architectures.
      </Typography>

      <Typography>
        To help you deal with these realities, Akka provides:
        <ol>
          <li>Multi-threaded behavior without the use of low-level concurrency constructs like atomics or locks — relieving you from even thinking about memory visibility issues.</li>
          <li>Transparent remote communication between systems and their components — relieving you from writing and maintaining difficult networking code.</li>
          <li>A clustered, high-availability architecture that is elastic, scales in or out, on demand — enabling you to deliver a truly reactive system.</li>
        </ol>
        Akka’s use of the actor model provides a level of abstraction that makes it easier to write correct concurrent, parallel and distributed systems. The actor model spans the full set of Akka libraries, providing you with a consistent way of understanding and using them. Thus, Akka offers a depth of integration that you cannot achieve by picking libraries to solve individual problems and trying to piece them together.
      </Typography>
      <Typography>
        By learning Akka and how to use the actor model, you will gain access to a vast and deep set of tools that solve difficult distributed/parallel systems problems in a uniform programming model where everything fits together tightly and efficiently.
      </Typography>
      <Typography id={motivation.id} variant={'title'}>
        {motivation.display}
      </Typography>
      <Typography>
        The actor model was proposed decades ago by <SimpleLink href="https://en.wikipedia.org/wiki/Carl_Hewitt#Actor_model">Carl Hewitt</SimpleLink> as a way to handle parallel processing in a high performance network — an environment that was not available at the time. Today, hardware and infrastructure capabilities have caught up with and exceeded Hewitt’s vision. Consequently, organizations building distributed systems with demanding requirements encounter challenges that cannot fully be solved with a traditional object-oriented programming (OOP) model, but that can benefit from the actor model.
      </Typography>
      <Typography>
        Today, the actor model is not only recognized as a highly effective solution — it has been proven in production for some of the world’s most demanding applications. To highlight issues that the actor model addresses, this topic discusses the following mismatches between traditional programming assumptions and the reality of modern multi-threaded, multi-CPU architectures:
        <ol>
          <li><SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/typed/guide/actors-motivation.html#the-challenge-of-encapsulation">The challenge of encapsulation</SimpleLink></li>
          <li><SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/typed/guide/actors-motivation.html#the-illusion-of-shared-memory-on-modern-computer-architectures">The illusion of shared memory on modern computer architectures</SimpleLink></li>
          <li><SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/typed/guide/actors-motivation.html#the-illusion-of-a-call-stack">The illusion of a call stack</SimpleLink></li>
        </ol>
        Read more about these challenges over here: <SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/typed/guide/actors-motivation.html">https://doc.akka.io/docs/akka/2.6.0/typed/guide/actors-motivation.html</SimpleLink>
      </Typography>
      <Typography id={actorModel.id} variant={'title'}>
        {actorModel.display}
      </Typography>
      <Typography>
        As mentioned above, common programming practices do not properly address the needs of demanding modern systems. Thankfully, we don’t need to scrap everything we know. Instead, the actor model addresses these shortcomings in a principled way, allowing systems to behave in a way that better matches our mental model. The actor model abstraction allows you to think about your code in terms of communication, not unlike the exchanges that occur between people in a large organization.
      </Typography>
      <Typography>
        Use of actors allows us to:
        <ol>
          <li>Enforce encapsulation without resorting to locks.</li>
          <li>Use the model of cooperative entities reacting to signals, changing state, and sending signals to each other to drive the whole application forward.</li>
          <li>Stop worrying about an executing mechanism which is a mismatch to our world view.</li>
        </ol>
      </Typography>
      <Typography variant="section">
        Usage of message passing avoids locking and blocking
      </Typography>
      <Typography>
        Instead of calling methods, actors send messages to each other. Sending a message does not transfer the thread of execution from the sender to the destination. An actor can send a message and continue without blocking. Therefore, it can accomplish more in the same amount of time.
      </Typography>
      <Typography>
        With objects, when a method returns, it releases control of its executing thread. In this respect, actors behave much like objects, they react to messages and return execution when they finish processing the current message. In this way, actors actually achieve the execution we imagined for objects:
      </Typography>
      <img className={classes.img} src={ActorGraph}/>
      <Typography>
        An important difference between passing messages and calling methods is that messages have no return value. By sending a message, an actor delegates work to another actor. As we saw in The illusion of a call stack, if it expected a return value, the sending actor would either need to block or to execute the other actor’s work on the same thread. Instead, the receiving actor delivers the results in a reply message.
      </Typography>
      <Typography>
        The second key change we need in our model is to reinstate encapsulation. Actors react to messages just like objects “react” to methods invoked on them. The difference is that instead of multiple threads “protruding” into our actor and wreaking havoc to internal state and invariants, actors execute independently from the senders of a message, and they react to incoming messages sequentially, one at a time. While each actor processes messages sent to it sequentially, different actors work concurrently with each other so that an actor system can process as many messages simultaneously as the hardware will support.
      </Typography>
      <Typography>
        Since there is always at most one message being processed per actor, the invariants of an actor can be kept without synchronization. This happens automatically without using locks:
      </Typography>
      <img className={classes.img} src={TimelineInvariants}/>
      <Typography>
        In summary, this is what happens when an actor receives a message:
        <ol>
          <li>The actor adds the message to the end of a queue.</li>
          <li>If the actor was not scheduled for execution, it is marked as ready to execute.</li>
          <li>A (hidden) scheduler entity takes the actor and starts executing it.</li>
          <li>Actor picks the message from the front of the queue.</li>
          <li>Actor modifies internal state, sends messages to other actors.</li>
          <li>The actor is unscheduled.</li>
        </ol>
        To accomplish this behavior, actors have:
        <ol>
          <li>A mailbox (the queue where messages end up).</li>
          <li>A behavior (the state of the actor, internal variables etc.).</li>
          <li>Messages (pieces of data representing a signal, similar to method calls and their parameters).</li>
          <li>An execution environment (the machinery that takes actors that have messages to react to and invokes their message handling code).</li>
          <li>An address (more on this later).</li>
        </ol>
        Messages go into actor mailboxes. The behavior of the actor describes how the actor responds to messages (like sending more messages and/or changing state). An execution environment orchestrates a pool of threads to drive all these actions completely transparently.
      </Typography>
      <Typography>
        This is a very simple model and it solves the issues enumerated previously:
        <ol>
          <li>Encapsulation is preserved by decoupling execution from signaling (method calls transfer execution, message passing does not).</li>
          <li>There is no need for locks. Modifying the internal state of an actor is only possible via messages, which are processed one at a time eliminating races when trying to keep invariants.</li>
          <li>There are no locks used anywhere, and senders are not blocked. Millions of actors can be efficiently scheduled on a dozen of threads reaching the full potential of modern CPUs. Task delegation is the natural mode of operation for actors.</li>
          <li>State of actors is local and not shared, changes and data is propagated via messages, which maps to how modern memory hierarchy actually works. In many cases, this means transferring over only the cache lines that contain the data in the message while keeping local state and data cached at the original core. The same model maps exactly to remote communication where the state is kept in the RAM of machines and changes/data is propagated over the network as packets.</li>
        </ol>
      </Typography>
      <Typography variant="section">
        Actors handle error situations gracefully
      </Typography>
      <Typography>
        Since we no longer have a shared call stack between actors that send messages to each other, we need to handle error situations differently. There are two kinds of errors we need to consider:
        <ol>
          <li>The first case is when the delegated task on the target actor failed due to an error in the task (typically some validation issue, like a non-existent user ID). In this case, the service encapsulated by the target actor is intact, it is only the task that itself is erroneous. The service actor should reply to the sender with a message, presenting the error case. There is nothing special here, errors are part of the domain and hence become ordinary messages.</li>
          <li>The second case is when a service itself encounters an internal fault. Akka enforces that all actors are organized into a tree-like hierarchy, i.e. an actor that creates another actor becomes the parent of that new actor. This is very similar how operating systems organize processes into a tree. Just like with processes, when an actor fails, its parent actor can decide how to react to the failure. Also, if the parent actor is stopped, all of its children are recursively stopped, too. This service is called supervision and it is central to Akka.</li>
        </ol>
        A supervisor strategy is typically defined by the parent actor when it is starting a child actor. It can decide to restart the child actor on certain types of failures or stop it completely on others. Children never go silently dead (with the notable exception of entering an infinite loop) instead they are either failing and the supervisor strategy can react to the fault, or they are stopped (in which case interested parties are notified). There is always a responsible entity for managing an actor: its parent. Restarts are not visible from the outside: collaborating actors can keep continuing sending messages while the target actor restarts.
      </Typography>
      <Typography id={toolkit.id} variant={'title'}>
        {toolkit.display}
      </Typography>
      <Typography>
        Before delving into some best practices for writing actors, it will be helpful to preview the most commonly used Akka libraries. This will help you start thinking about the functionality you want to use in your system. All core Akka functionality is available as Open Source Software (OSS). Lightbend sponsors Akka development but can also help you with commercial offerings such as training, consulting, support, and Enterprise Suite — a comprehensive set of tools for managing Akka systems.
      </Typography>

      <Typography>
        The following capabilities are included with Akka OSS:
        <ol>
          <li><SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/typed/guide/modules.html#actor-library">Actor library</SimpleLink></li>
          <li><SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/typed/guide/modules.html#remoting">Remoting</SimpleLink></li>
          <li><SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/typed/guide/modules.html#cluster">Cluster</SimpleLink></li>
          <li><SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/typed/guide/modules.html#cluster-sharding">Cluster Sharding</SimpleLink></li>
          <li><SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/typed/guide/modules.html#cluster-singleton">Cluster Singleton</SimpleLink></li>
          <li><SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/typed/guide/modules.html#persistence">Persistence</SimpleLink></li>
          <li><SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/typed/guide/modules.html#distributed-data">Distributed Data</SimpleLink></li>
          <li><SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/typed/guide/modules.html#streams">Streams</SimpleLink></li>
          <li><SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/typed/guide/modules.html#http">HTTP</SimpleLink></li>
        </ol>
        With a <SimpleLink href="https://www.lightbend.com/lightbend-subscription">Lightbend Platform Subscription</SimpleLink>, you can use <SimpleLink href="https://doc.akka.io/docs/akka-enhancements/current/">Akka Enhancements</SimpleLink> that includes:
      </Typography>
      <Typography>
        <SimpleLink href="https://doc.akka.io/docs/akka-enhancements/current/akka-resilience-enhancements.html">Akka Resilience Enhancements</SimpleLink>:
        <ol>
          <li><SimpleLink href="https://doc.akka.io/docs/akka-enhancements/current/split-brain-resolver.html">Split Brain Resolver</SimpleLink> — Detects and recovers from network partitions, eliminating data inconsistencies and possible downtime.</li>
          <li><SimpleLink href="https://doc.akka.io/docs/akka-enhancements/current/config-checker.html">Configuration Checker</SimpleLink> — Checks for potential configuration issues and logs suggestions.</li>
          <li><SimpleLink href="https://doc.akka.io/docs/akka-enhancements/current/diagnostics-recorder.html">Diagnostics Recorder</SimpleLink> — Captures configuration and system information in a format that makes it easy to troubleshoot issues during development and production.</li>
          <li><SimpleLink href="https://doc.akka.io/docs/akka-enhancements/current/starvation-detector.html">Thread Starvation Detector</SimpleLink> — Monitors an Akka system dispatcher and logs warnings if it becomes unresponsive.</li>
          <li><SimpleLink href="https://doc.akka.io/docs/akka-enhancements/current/kubernetes-lease.html">Kubernetes Lease</SimpleLink> — Monitors an Akka system dispatcher and logs warnings if it becomes unresponsive.</li>
          <li><SimpleLink href="https://doc.akka.io/docs/akka-enhancements/current/fast-failover.html">Fast Failover</SimpleLink> — Fast failover for Cluster Sharding.</li>
        </ol>
        <SimpleLink href="https://doc.akka.io/docs/akka-enhancements/current/akka-persistence-enhancements.html">Akka Persistence Enhancements</SimpleLink>:
        <ol>
          <li><SimpleLink href="https://doc.akka.io/docs/akka-enhancements/current/persistence-dc/index.html">Multi-DC Persistence — For active-active persistent entities across multiple data centers.</SimpleLink></li>
          <li><SimpleLink href="https://doc.akka.io/docs/akka-enhancements/current/gdpr/index.html">GDPR for Akka Persistence — Data shredding can be used to forget information in events.</SimpleLink></li>
        </ol>
      </Typography>
    </Fragment>
  )
}

export default withStyles(styles)(AkkaIntro)

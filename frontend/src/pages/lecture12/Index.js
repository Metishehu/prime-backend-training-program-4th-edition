/**
 * Created by LeutrimNeziri on 09/04/2019.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import React from "react";
import WhatNext from "pages/lecture12/WhatNext";

const styles = ({ typography }) => ({
  root: {},
})

class Index extends React.Component {
  render() {
    const { classes, breadcrumbs, ...other } = this.props

    let section = breadcrumbs[0]
    if (breadcrumbs.length > 1) {
      section = breadcrumbs[1]
    }

    const props = {
      section,
      ...other
    }
    return <WhatNext {...props}/>
  }
}

export default withStyles(styles)(Index)

/**
 * Created by Agon Lohaj on 27/08/2020.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import SimpleLink from "presentations/rows/SimpleLink";
import { Bold, Highlighted } from "presentations/Label";
import Code from "presentations/Code";

const styles = ({ typography }) => ({
  root: {},
})

const verboseAction = `public class VerboseAction extends Action.Simple {
  @Override
  public CompletionStage<Result> call(Http.Request request) {
		Logger.of(this.getClass()).debug("Just logging while being called for {}", request);
		return delegate.call(request);
  }
}`

const withAnnotation = `@With(VerboseAction.class)
public CompletableFuture<Result> index() {
  return CompletableFuture.supplyAsync(() -> ok(views.html.index.render(config)), ec.current());
}`

const mixingItUp = `@Security.Authenticated
@Cached(key = "index.result")
public Result authenticatedCachedIndex() {
  return ok("It works!");
}`

const verboseAnnotation = `@With(VerboseAnnotationAction.class)
@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface VerboseAnnotation {
	boolean value() default true;
}`

const verboseAnnotatedAction = `public class VerboseAnnotationAction extends Action<VerboseAnnotation> {

	@Override
    public CompletionStage<Result> call(Http.Request request) {
		if (configuration.value()) {
			Logger.of(this.getClass()).debug("Just logging conditionaly based on my configurated value while being called for {}", request);
		}
		return delegate.call(request);
    }
}`

const verboseAnnotationActionUsage = `@VerboseAnnotation( false )
public CompletableFuture<Result> index() {
  return CompletableFuture.supplyAsync(() -> ok(views.html.index.render(config)), ec.current());
}`

const contollerLevelAnnotation =`@Security.Authenticated
public class Admin extends Controller {
...
}`

const actionAttributes = `public class Attributes {
	public static final TypedKey<Taxi> TAXI_TYPED_KEY = TypedKey.<Taxi>create("taxi");
}

public class TaxiProviderAction extends Action.Simple {

	@Override
    public CompletionStage<Result> call(Http.Request request) {
      request = request.addAttr(Attributes.TAXI_TYPED_KEY, new Taxi());
      return delegate.call(request);
    }
}`

const consumeAttribute = `@With( TaxiProviderAction.class )
public Result taxi (Http.Request request) {
  Taxi taxi = request.attrs().get(Attributes.TAXI_TYPED_KEY);
  return ok(Json.toJson(taxi));
}`

const withCacheAnnotation = `@With(WithCacheAction.class)
@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface WithCache {
	String key();
}`

const withCacheAction = `public class WithCacheAction extends Action<WithCache> {

	@Inject
	@NamedCache("training")
	AsyncCacheApi cacheApi;

	@Override
    public CompletionStage<Result> call(Http.Request request) {
		Logger.of(this.getClass()).debug("Cached Action for {}. Cache Api exists: {}", configuration.key(), cacheApi != null ? "Not null" : "null");
		return cacheApi.getOrElseUpdate(configuration.key(), () -> delegate.call(request));
    }
}`

const withCacheExample = `@With( TaxiProviderAction.class )
@WithCache(key = "taxi")
public Result cachedTaxi (Http.Request request) {
  Logger.of(this.getClass()).debug("Calculating taxi for the cache Action");
  Taxi taxi = request.attrs().get(Attributes.TAXI_TYPED_KEY);
  return ok(Json.toJson(taxi));
}`


const compileTimeDependency = `public class MyComponents extends BuiltInComponentsFromContext
    implements NoHttpFiltersComponents, CaffeineCacheComponents {

  public MyComponents(ApplicationLoader.Context context) {
    super(context);
  }

  @Override
  public Router router() {
    return Router.empty();
  }

  @Override
  public MappedJavaHandlerComponents javaHandlerComponents() {
    return super.javaHandlerComponents()
        // Add action that does not depends on any other component
        .addAction(VerboseAction.class, VerboseAction::new)
        // Add action that depends on the cache api
        .addAction(MyOwnCachedAction.class, () -> new MyOwnCachedAction(defaultCacheApi()));
  }
}`

class ActionComposition extends React.Component {
  render() {
    const { classes, section } = this.props
    const reminder = section.children[0]
    const composingActions = section.children[1]
    const definingCustomActionAnnotations = section.children[2]
    const annotatingControllers = section.children[3]
    const passingAttributesFromActionToController = section.children[4]
    const usingDependencyInjection = section.children[5]
    return (
      <Fragment>
        <Typography variant={'heading'}>
          Section 5: {section.display}
          <Typography>
            Resources: <SimpleLink href="https://www.playframework.com/documentation/2.8.x/JavaActionsComposition">https://www.playframework.com/documentation/2.8.x/JavaActionsComposition</SimpleLink>
          </Typography>
          <Typography>
            This section introduces several ways to define generic action functionality.
          </Typography>
          <Divider />
        </Typography>
        <Typography id={reminder.id} variant={'title'}>
          {reminder.display}
        </Typography>
        <Typography>
          Previously, we said that an action is a Java method that returns a <Highlighted>play.mvc.Result</Highlighted> value. Actually, Play manages internally actions as functions. An action provided by the Java API is an instance of <Highlighted>play.mvc.Action</Highlighted>. Play builds a root action for you that just calls the proper action method. This allows for more complicated action composition.
        </Typography>
        <Typography id={composingActions.id} variant={'title'}>
          {composingActions.display}
        </Typography>
        <Typography>
          Here is the definition of a custom action called <Highlighted>VerboseAction</Highlighted>:
          <Code>
            {verboseAction}
          </Code>
          You can compose the code provided by the action method with another <Highlighted>play.mvc.Action</Highlighted>, using the <Highlighted>@With</Highlighted> annotation:
          <Code>
            {withAnnotation}
          </Code>
          At one point you need to delegate to the wrapped action using <Highlighted>delegate.call(...)</Highlighted>.
        </Typography>
        <Typography>
          You also mix several actions by using custom action annotations:
          <Code>
            {mixingItUp}
          </Code>
        </Typography>
        <Typography id={definingCustomActionAnnotations.id} variant={'title'}>
          {definingCustomActionAnnotations.display}
        </Typography>
        <Typography>
          You can also mark action composition with your own annotation, which must itself be annotated using <Highlighted>@With</Highlighted>:
          <Code>
            {verboseAnnotation}
          </Code>
          Your <Highlighted>Action</Highlighted> definition retrieves the annotation as configuration:
          <Code>
            {verboseAnnotatedAction}
          </Code>
          You can then use your new annotation with an action method:
          <Code>
            {verboseAnnotationActionUsage}
          </Code>
        </Typography>
        <Typography id={annotatingControllers.id} variant={'title'}>
          {annotatingControllers.display}
        </Typography>
        <Typography>
          You can also put any action composition annotation directly on the <Highlighted>Controller</Highlighted> class. In this case it will be applied to all action methods defined by this controller:
          <Code>
            {contollerLevelAnnotation}
          </Code>
          <Highlighted>
            <Bold>Note</Bold>: If you want the action composition annotation(s) put on a Controller class to be executed before the one(s) put on action methods set <Bold>play.http.actionComposition.controllerAnnotationsFirst = true</Bold> in application.conf (which is currently the case). However, be aware that if you use a third party module in your project it may rely on a certain execution order of its annotations.
          </Highlighted>
        </Typography>
        <Typography id={passingAttributesFromActionToController.id} variant={'title'}>
          {passingAttributesFromActionToController.display}
        </Typography>
        <Typography>
          You can pass an object from an action to a controller by utilizing request attributes.
          <Code>
            {actionAttributes}
          </Code>
          Then in an action you can get the request attribute like this:
          <Code>
            {consumeAttribute}
          </Code>
        </Typography>

        <Typography id={usingDependencyInjection.id} variant={'title'}>
          {usingDependencyInjection.display}
        </Typography>
        <Typography>
          You can use runtime Dependency Injection or compile-time Dependency Injection together with action composition.
        </Typography>
        <Typography variant="section">
          Runtime Dependency Injection
        </Typography>
        <Typography>
          For example, if you want to define your own result cache solution, first define the annotation:
          <Code>
            {withCacheAnnotation}
          </Code>
          And then you can define your action with the dependencies injected:
          <Code>
            {withCacheAction}
          </Code>
          And compose it like this:
          <Code>
            {withCacheExample}
          </Code>
        </Typography>

        <Typography variant="section">
          Compile-time Dependency Injection
        </Typography>
        <Typography>
          When using compile-time Dependency Injection, you need to manually add your Action supplier to JavaHandlerComponents. You do that by overriding method javaHandlerComponents in BuiltInComponents:
          <Code>
            {compileTimeDependency}
          </Code>
          <Highlighted>
            Note: As stated above, every request must be served by a distinct instance of your play.mvc.Action and that is why you add a {`java.util.function.Supplier<Action>`} instead of the instance itself. Of course, you can have a Supplier returning the same instance every time, but this is not encouraged.
          </Highlighted>
        </Typography>

      </Fragment>
    )
  }
}

export default withStyles(styles)(ActionComposition)

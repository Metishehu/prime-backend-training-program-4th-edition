/**
 * Created by Agon Lohaj on 27/08/2020.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import SimpleLink from "presentations/rows/SimpleLink";
import { Highlighted } from "presentations/Label";
import Code from "presentations/Code";
import WebSocket from 'presentations/WebSocket'

const styles = ({ size, typography }) => ({
  root: {}
})


const chatActor = `/**
 * Chat Actor - Representing a user in a room!
 */
public class ChatActor extends AbstractActor {
	/**
	 * For logging purposes
	 */
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

	/**
	 * String messages as constants
	 */
	private static final String JOINED_ROOM = "Someone Joined the Room!";
	private static final String LEFT_ROOM = "Someone Left the Room!";
	private static final String PING = "PING";
	private static final String PONG = "PONG";

	/**
	 * Mediator
	 */
	private ActorRef mediator = DistributedPubSub.get(getContext().system()).mediator();
	/**
	 * Room ID to pub/sub
	 */
	private String roomId;
	/**
	 * Web socket represented from the front end
	 */
	private ActorRef out;

	public static Props props(ActorRef out, String roomId) {
		return Props.create(ChatActor.class, () -> new ChatActor(out, roomId));
	}

	private ChatActor(ActorRef out, String roomId) {
		this.roomId = roomId;
		this.out = out;
		mediator.tell(new DistributedPubSubMediator.Subscribe(roomId, getSelf()), getSelf());
	}

	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(String.class, this::onMessageReceived)
				.match(ChatActorProtocol.ChatMessage.class, this::onChatMessageReceived)
				.match(DistributedPubSubMediator.SubscribeAck.class, this::onSubscribe)
				.match(DistributedPubSubMediator.UnsubscribeAck.class, this::onUnsubscribe)
				.build();
	}

	/**
	 * Receiver of socket messages comming from the front end
	 * @param message
	 */
	public void onMessageReceived (String message) {
		if (message.equals(PING)) {
			out.tell(PONG, getSelf());
			return;
		}
		broadcast(message);
	}

	/**
	 * Chat Message Protocol message receiver
	 * @param what
	 */
	public void onChatMessageReceived (ChatActorProtocol.ChatMessage what) {
		// Don't send messages back that came from this socket
		if (getSender().equals(getSelf())) {
			return;
		}
		String message = what.getMessage();
		out.tell(message, getSelf());
	}

	/**
	 * When a subscribe message is received, this method gets called
	 * @param message
	 */
	public void onSubscribe (DistributedPubSubMediator.SubscribeAck message) {
		this.joinTheRoom();
	}

	/**
	 * When an unsubscribe message is received, this method gets called
	 * @param message
	 */
	public void onUnsubscribe (DistributedPubSubMediator.UnsubscribeAck message) {
		this.leaveTheRoom();
	}

	/**
	 * When the actor is shutting down, let the others know that I've left the room!
	 */
	@Override
	public void postStop() {
		this.leaveTheRoom();
	}

	/**
	 * Sends a simple JOINED_ROOM message
	 */
	private void joinTheRoom () {
		this.broadcast(JOINED_ROOM);
	}

	/**
	 * Sends a simple LEFT_ROOM message
	 */
	private void leaveTheRoom () {
		this.broadcast(LEFT_ROOM);
	}

	/**
	 * Publish message to the current room
	 * @param message
	 */
	private void broadcast (String message) {
		// Publish new content on this room!
		mediator.tell(
			new DistributedPubSubMediator.Publish(roomId, new ChatActorProtocol.ChatMessage(message)),
			getSelf()
		);
	}
}`

const chatActorUsage = `@Singleton
public class LectureEightController extends Controller {
	@Inject
	private ActorSystem actorSystem;
	@Inject
	private Materializer materializer;

	public WebSocket chat (String room) {
		return WebSocket.Text.accept(request -> ActorFlow.actorRef((out) -> ChatActor.props(out, room), actorSystem, materializer));
	}

	@BodyParser.Of(BodyParser.Json.class)
	public Result publish (Http.Request request, String room) {
		JsonNode node = request.body().asJson();
		Cluster cluster = Cluster.get(actorSystem);
		ActorRef mediator = DistributedPubSub.get(cluster.system()).mediator();
		mediator.tell(
				new DistributedPubSubMediator.Publish(room, new ChatActorProtocol.ChatMessage(node.asText())),
				ActorRef.noSender()
		);
		return ok(node);
	}
}`

const destination = `static class Destination extends AbstractActor {
  LoggingAdapter log = Logging.getLogger(getContext().system(), this);

  public Destination() {
    ActorRef mediator = DistributedPubSub.get(getContext().system()).mediator();
    // register to the path
    mediator.tell(new DistributedPubSubMediator.Put(getSelf()), getSelf());
  }

  @Override
  public Receive createReceive() {
    return receiveBuilder().match(String.class, msg -> log.info("Got: {}", msg)).build();
  }
}`

const initialisation = `system.actorOf(Props.create(Destination.class), "destination");
// another node
system.actorOf(Props.create(Destination.class), "destination");`

const senderActor = `static class Sender extends AbstractActor {

  // activate the extension
  ActorRef mediator = DistributedPubSub.get(getContext().system()).mediator();

  @Override
  public Receive createReceive() {
    return receiveBuilder()
        .match(
            String.class,
            in -> {
              String out = in.toUpperCase();
              boolean localAffinity = true;
              mediator.tell(
                  new DistributedPubSubMediator.Send("/user/destination", out, localAffinity),
                  getSelf());
            })
        .build();
  }
}`

const messagesFromSomewhereElse = `// somewhere else
ActorRef sender = system.actorOf(Props.create(Publisher.class), "sender");
// after a while the destinations are replicated
sender.tell("hello", null);`


const webSocketStyles = ({ size, palette, typography }) => ({
  messages: {
    height: 320,
    background: 'white',
    width: '100%',
    overflow: 'auto',
    position: 'relative'
  },
  row: {
    marginBottom: 8,
    fontSize: size.defaultFontSize,
    color: palette.textColor
  },
  actions: {
    display: 'flex',
    flexFlow: 'row nowrap',
    alignItems: 'center',
    marginBottom: 8,
    width: 480,
    justifyContent: 'space-between',
    '& :first-child': {
      marginRight: 4,
      flex: 1
    },
    '& :last-child': {
      marginRight: 4,
    },
  },
  send: {
    color: palette.leadColor,
    fontWeight: 'bold'
  },
  received: {
    color: palette.success,
  }
})

const AkkaPubSub = (props) => {
  const { classes, section } = props
  const introduction = section.children[0]
  const publish = section.children[1]
  const send = section.children[2]
  const playground = section.children[3]

  return (
    <Fragment>
      <Typography variant={'heading'}>
        Section 8: {section.display}
        <Typography>
          Resources: <ol>
          <li><SimpleLink href="https://akka.io/">https://akka.io/</SimpleLink></li>
          <li><SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/distributed-pub-sub.html">https://doc.akka.io/docs/akka/2.6.0/distributed-pub-sub.html</SimpleLink></li>
        </ol>

        </Typography>
        <Divider />
      </Typography>
      <Typography id={introduction.id} variant={'title'}>
        {introduction.display}
      </Typography>
      <Typography>
        The simple idea behind publish subscribe is that for a topic, actors can publish data and also subscribe to incomming data within that topic. In a distributed cluster, this means that using this mechanism actors can push events and react to them using the Akka Pub/Sub
      </Typography>
      <Typography>
        So:
        <ol>
          <li>How do I send a message to an actor without knowing which node it is running on?</li>
          <li>How do I send messages to all actors in the cluster that have registered interest in a named topic?</li>
        </ol>
        This pattern provides a mediator actor, akka.cluster.pubsub.DistributedPubSubMediator, that manages a registry of actor references and replicates the entries to peer actors among all cluster nodes or a group of nodes tagged with a specific role.
      </Typography>
      <Typography>
        The DistributedPubSubMediator actor is supposed to be started on all nodes, or all nodes with specified role, in the cluster. The mediator can be started with the DistributedPubSub extension or as an ordinary actor.
      </Typography>
      <Typography>
        The registry is eventually consistent, i.e. changes are not immediately visible at other nodes, but typically they will be fully replicated to all other nodes after a few seconds. Changes are only performed in the own part of the registry and those changes are versioned. Deltas are disseminated in a scalable way to other nodes with a gossip protocol.
      </Typography>
      <Typography>
        Cluster members with status <SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/typed/cluster-membership.html#weakly-up">WeaklyUp</SimpleLink>, will participate in Distributed Publish Subscribe, i.e. subscribers on nodes with WeaklyUp status will receive published messages if the publisher and subscriber are on same side of a network partition.
      </Typography>
      <Typography>
        You can send messages via the mediator on any node to registered actors on any other node.
      </Typography>
      <Typography>
        There a two different modes of message delivery, explained in the sections Publish and Send below.
      </Typography>

      <Typography id={publish.id} variant={'title'}>
        {publish.display}
      </Typography>
      <Typography>
        This is the true pub/sub mode. A typical usage of this mode is a chat room in an instant messaging application.
      </Typography>
      <Typography>
        Actors are registered to a named topic. This enables many subscribers on each node. The message will be delivered to all subscribers of the topic.
      </Typography>
      <Typography>
        For efficiency the message is sent over the wire only once per node (that has a matching topic), and then delivered to all subscribers of the local topic representation.
      </Typography>
      <Typography>
        You register actors to the local mediator with <Highlighted>DistributedPubSubMediator.Subscribe</Highlighted>. Successful Subscribe and Unsubscribe is acknowledged with <Highlighted>DistributedPubSubMediator.SubscribeAck</Highlighted> and <Highlighted>DistributedPubSubMediator.UnsubscribeAck</Highlighted> replies. The acknowledgment means that the subscription is registered, but it can still take some time until it is replicated to other nodes.
      </Typography>
      <Typography>
        You publish messages by sending <Highlighted>DistributedPubSubMediator.Publish</Highlighted> message to the local mediator.
      </Typography>
      <Typography>
        Actors are automatically removed from the registry when they are terminated, or you can explicitly remove entries with <Highlighted>DistributedPubSubMediator.Unsubscribe</Highlighted>.
      </Typography>
      <Typography>
        Here is our example Chat Actor (Implemented at the backend):
        <Code>
          {chatActor}
        </Code>
        And here is how we use it:
        <Code>
          {chatActorUsage}
        </Code>
      </Typography>
      <Typography variant="section">
        Topic Groups
      </Typography>
      <Typography>
        Actors may also be subscribed to a named topic with a group id. If subscribing with a group id, each message published to a topic with the sendOneMessageToEachGroup flag set to true is delivered via the supplied RoutingLogic (default random) to one actor within each subscribing group.
      </Typography>
      <Typography>
        If all the subscribed actors have the same group id, then this works just like <Highlighted>Send</Highlighted> and each message is only delivered to one subscriber.
      </Typography>
      <Typography>
        If all the subscribed actors have different group names, then this works like normal Publish and each message is broadcasted to all subscribers.
      </Typography>

      <Typography id={send.id} variant={'title'}>
        {send.display}
      </Typography>
      <Typography>
        In contrast to Publish, this is a point-to-point mode where each message is delivered to one destination, but you still do not have to know where the destination is located. A typical usage of this mode is private chat to one other user in an instant messaging application. It can also be used for distributing tasks to registered workers, like a cluster aware router where the routees dynamically can register themselves.
      </Typography>
      <Typography>
        The message will be delivered to one recipient with a matching path, if any such exists in the registry. If several entries match the path because it has been registered on several nodes the message will be sent via the supplied RoutingLogic (default random) to one destination. The sender of the message can specify that local affinity is preferred, i.e. the message is sent to an actor in the same local actor system as the used mediator actor, if any such exists, otherwise route to any other matching entry.
      </Typography>
      <Typography>
        You register actors to the local mediator with <Highlighted>DistributedPubSubMediator.Put</Highlighted>. The <Highlighted>ActorRef</Highlighted> in <Highlighted>Put</Highlighted> must belong to the same local actor system as the mediator. The path without address information is the key to which you send messages. On each node there can only be one actor for a given path, since the path is unique within one local actor system.
      </Typography>
      <Typography>
        You send messages by sending <Highlighted>DistributedPubSubMediator.Send</Highlighted> message to the local mediator with the path (without address information) of the destination actors.
      </Typography>
      <Typography>
        Actors are automatically removed from the registry when they are terminated, or you can explicitly remove entries with <Highlighted>DistributedPubSubMediator.Remove</Highlighted>.
      </Typography>
      <Typography>
        An example of a destination actor:
        <Code>
          {destination}
        </Code>
        Destination actors can be started on several nodes in the cluster, and all will receive messages sent to the path (without address information).
        <Code>
          {initialisation}
        </Code>
        A simple actor that sends to the path:
        <Code>
          {senderActor}
        </Code>
        It can send messages to the path from anywhere in the cluster:
        <Code>
          {messagesFromSomewhereElse}
        </Code>
        It is also possible to broadcast messages to the actors that have been registered with <Highlighted>Put</Highlighted>. Send <Highlighted>DistributedPubSubMediator.SendToAll</Highlighted> message to the local mediator and the wrapped message will then be delivered to all recipients with a matching path. Actors with the same path, without address information, can be registered on different nodes. On each node there can only be one such actor, since the path is unique within one local actor system.
      </Typography>
      <Typography>
        Typical usage of this mode is to broadcast messages to all replicas with the same path, e.g. 3 actors on different nodes that all perform the same actions, for redundancy. You can also optionally specify a property (allButSelf) deciding if the message should be sent to a matching path on the self node or not.
      </Typography>

      <Typography id={playground.id} variant={'title'}>
        {playground.display}
      </Typography>
      <Typography>
        Using the implementation above, we can now mess around with the web-socket available for us:
      </Typography>
      <WebSocket endpoint={'/lecture8/chat/frontend'}/>
    </Fragment>
  )
}

export default withStyles(styles)(AkkaPubSub)

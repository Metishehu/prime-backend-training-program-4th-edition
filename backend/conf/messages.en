title=PlayStartApp

invalid_parameters=Oops. Your request could not be processed. Please check your settings. If you keep getting this error, then contact your system administrator for support.
service_unavailable=Oops. This service is currently not available. Please try again later. If you keep getting this error, then please contact your system administrator for support.
failed_query=Oops. The request failed. Try again later. If you keep getting this error, please contact your system administrator for support.
service_failed=Oops. This page is currently not available. Please try again later. If you keep getting this error, then please contact your system administrator for support.
service_timeout=Oops. The internet connection is too slow to load this page. Please try again later. If you keep getting this error, then please contact your system administrator for support. 

token_expired=Oops!. The activation period has expired. Please ask your system administrator to resend the invite.
access_forbidden=Oops! You don't have permission to perform this request. Please contact your system administrator if you believe you should have access.
conflict=The resource you are trying to modify has already been changed by another user!
access_missing=No access has been assigned! This field is required!

#totop strings
totp_missing=Code missing
topt_bad=Invalid Code
cannot_find_scratch_code=The backup code is invalid

access_for_collection_exists=Access for the specified resource already exists!

#Files related strings
unsupported_file_type = File type not supported

# data source related strings
data_source_id_missing=The data source ID is missing
prefix_exists=This data source topic is already in use! Please provide another topic name.
no_file_selected=No file selected
expected_array_data=Expected the datasource to contain an array of values
not_found=Resource not found
count=Count
keys_missing=The following field(s) are missing or they have been migrated due to incompatible Data Source Version: {0}
keys_denied=Access denied on the following field(s): {0}
bad_code=The following computed field: {0} contains invalid code!
datasource_denied=The following data source: {0} at field: {1} isn't allowed!
options_missing=The following configuration option(s) are missing: {0}
no_subscribers = There are not any subscribers at this datasource. 
edit_column_forbidden=Changing the key or type of this column is not allowed!
delete_column_forbidden=Removing this column is not allowed!

# entity related strings
version_id_required = The version id is required
datasource_id_required = The datasource id is required
user_id_required = The user id is required
status_is_required = The user status is required.
datasource_is_locked = You cannot do further changes in this dataset, because it's already locked!

# data related strings
type_missing=Type missing
dimensions_missing=No fields configured yet! Assign fields via the configuration pane on the right.
# Projects related strings
name_en_required=Display name for english required
name_nl_required=Display name for dutch required
datasource_required=Datasource required

# Channels related strings
context_required = Context is required
channel_exists= Group already exists
type_not_handled = Type can not be handled

# role assignment strings
invalid_collection_name=The collection name is not valid
editing_collection_not_allowed=You have no access to edit this collection

# mdp related strings
config_missing=Configuration is missing
file_missing=File cannot be processed

# alerts related strings
type_required=The type of the task is required
frequency_required=The frequency of the task is required
metric_required=The metric of the task is required
value_required=The value to compare it against is required
otherMetric_required=The metric to compare it against is required
serie_required = The serie of the task is required
otherSerie_required = The serie to compare it against is required
topic_exists = Alert topic already exists
topic_required = Alert topic is required

# users strings
old_password_incorrect=Old password is incorrect
username_exists=User name is already taken
email_exists=Email already exists
name_required=Name is required
first_name_required= First name is required
last_name_required= Last name is required
username_required=User name is required
email_required=Email is required
passwords_dont_match=Passwords don''t match
password_required=Password required
invalid_password=The password entered is invalid. It must be of at least 8 characters long!
same_password = Your old password can not be your new password
incorrect_username = The username should contain only one word

# Invites related strings
user_type_required = User type is required
roles_required = User must have at least one Group assigned!

# Feedback related strings
comment_required = The comment field is required

#Ticket related strings
ticket_title_req = Ticket title is required.
ticket_desc_req = Ticket description is required.
ticket_topic_req = Ticket type of problem is required.
only_admin_change_status = Only Admin user can change ticket status.
ticket_id_required = Ticket id is required.
text_required = Comment text is required.
#columns related strings
field_required = Field is required
character_length_exceeded = The maximum number of characters allowed is {1}
malformed=Field is malformed, expected it to be {0}
invalid_field = The value is not valid
numeric_within_value =  The value of the field should be {0} {1}
numeric_between_value =  The value of the field should be between {0} and {1} {2}

# setup strings
server_set=The server is already set up
server_unreachable=Oops. This page is currently not available. Please try again later. If you keep getting this error, then please contact your system administrator for support.

# t-lite related strings
wrong_number_inputs = Only {0} inputs allowed for this function.
wrong_type = The following key {0} needs to be of input type {1}
date_unparsable = Oops. We could not load the requested date. Please select another date.
arithmetic_exception = Could not perform {0} because of parsing exception. 
wrong_input_type = Could not perform the given operation because of wrong input type.


# general errors
parsing_exception=Parsing exception

# notification related strings
user_created_channel= {0} created group {1}
user_updated_channel = {0} updated group {1}
user_invited_user =  {0} invited {1} in group {2}
user_left_channel = {0} left group {1}
user_left_private_channel = {0} left the channel
user_joined_channel = {0} joined group {1}
direct_chat = This is the beginning of the private conversation
user_created_task = {0} created task {1}
user_updated_task = {0} updated task {1}
user_removed_user = {0} removed {1} from group {2}

mail.confirm.subject=Welcome to PlayStartApp - Ask for confirmation
mail.confirm.message=Welcome to PlayStartApp. <br> Click on this link : {0} to confirm your email.

mail.welcome.subject=Welcome to PlayStartApp
mail.welcome.message=Welcome to PlayStartApp. <br> Your account is validated. Enjoy !

mail.reset.ask.subject=PlayStartApp - New password
# Cannot use You've unescaped here, as the single quote stops the {0}
mail.reset.ask.message=You asked for your password to be reset.  <br> Click on this link : {0} to change your password.

mail.change.ask.subject=PlayStartApp - Validate email
mail.change.ask.message=You asked for your email to be changed.  <br> Click on this link : {0} to validate your email.

mail.reset.fail.subject=Account Access Attempted
mail.reset.fail.message=You (or someone else) entered this email address when trying to change the password.  However, this email address is not on our list of registered users, and so the attempted password reset has failed.

mail.reset.confirm.subject=PlayStartApp - New password
mail.reset.confirm.message=Hello, <br><br> Your password has been reset.

#email related strings and notifications
user_invited_user_email_subject = [PRIME] You have been invited to the chat channel {0}
user_invited_user_email_body = User <strong>{0}</strong> has invited you to collaborate on the <strong>{1}</strong> chat channel.
user_invited_user_cover_title = Invitation <br/> to collaborate
user_invited_user_button_title = Accept Invitation

alert_triggered_subject = [PRIME] Alert {0} has been triggered.
alert_triggered_body = Alert <strong>{0}</strong> has been triggered.
alert_triggered_cover_title = Alert <br>Triggered.
alert_triggered_button_title = Check alert

user_invited_accepted_prime_subject = [PRIME] User {0} has accepted your invitation
user_invited_user_accepted_email_body =  Good news! User <strong>{0}</strong> has accepted your invitation to join PRIME. You can now collaborate with <strong>{1}</strong> on PRIME.
user_invited_accepted_cover = User Accepted <br> Invitation
user_invited_accepted_button_tittle = Go to user management



user_invited_prime_subject =  [PRIME] You have been invited to join PRIME at {0}
user_invited_prime_first =  Your system administrator has invited you to join PRIME.
user_invited_prime_second =   You have been invited to join the PRIME platform. <br><br>PRIME is the intuitive analytics platform for all business professionals. We help you gain insight from data and drive business value, so that you can maximize your potential. For more information, visit <a href="https://goprime.io">goprime.io</a>
user_invited_prime_cover_title = Invitation to <br> join PRIME platform
user_invited_title = Hi there,
user_invited_button_title = Create my account
user_invited_second_paragraph = Please note that the activation link will expire in 5 days. <br>If you encounter any difficulties, then please get in touch with your system administrator.<br><br> <br/>Cheers,<br/>The team at PRIME

user_reset_password_subject = [PRIME] Password reset
user_reset_password_first_paragraph = Password reset.
user_reset_password_pharagraph =  Click on the following link to log in with the new password: <br/> {0} <br/><br/>.
user_reset_password_cover_title = Your password has been <br/> successfully reset
user_reset_password_second_paragraph =  If you did not request a password reset, please contact your system administrator.
user_reset_password_button_title = Log in


user_password_request_reset_cover_title = Password reset <br> request
user_password_request_reset_subject = [PRIME] Password reset
button_title = Reset password
user_password_request_reset_paragraph1 = You have requested a password reset.
user_password_request_reset_paragraph2 =  If you did not request a password reset, please contact your system administrator.

user_userStory_subject = [PRIME] You have been invited to a Story
user_userStory_button_title = Go to story
user_userStory_cover_title = Invited to <br> a story
user_userStory_paragraph = User <strong>{0}</strong> invited you to collaborate on a story at {1}.<br><br> Story name: <strong>{2}</strong>, <br/> Story description: <strong>{3}</strong>
user_userStory_paragraph_no_desc = User <strong>{0}</strong> invited you to collaborate on a story at {1}.<br><br> Story name: <strong>{2}</strong>

user_userStoryTemplate_subject = [PRIME] You have gained access to a new template
user_userStoryTemplate_button_title = See template
user_userStoryTemplate_cover_title = New template <br> available
user_userStoryTemplate_paragraph = User <strong>{0}</strong> granted access for you, now you can use this template to create stories at {1}.<br><br> Template name: <strong>{2}</strong>, <br/> Template description: <strong>{3}</strong>
user_userStoryTemplate_paragraph_no_desc = User <strong>{0}</strong> granted access for you, now you can use this template to create stories at {1}.<br><br> Template name: <strong>{2}</strong>

user_workSpace_subject = [PRIME] Invited to a project
user_workSpace_button_title = Go to project
user_workSpace_cover_title = Invited to <br> a project
user_workSpace_paragraph = User <strong>{0}</strong> invited you to collaborate on a project at {1}.<br><br> Project name: <strong>{2}</strong>, <br/>Project description: <strong>{3}</strong>
user_workSpace_paragraph_no_desc = User <strong>{0}</strong> invited you to collaborate on a project at {1}.<br><br> Project name: <strong>{2}</strong>

#<Ticket related emails>
user_added_ticket_subject = [PRIME] New ticket added
user_added_ticket_button_title = Check ticket
user_added_ticket_cover_title = New ticket <br> added
user_added_ticket_paragraph =  User <strong>{0}</strong> added a new ticket with the following text: <br/><br/> <strong>Title:</strong> {1}, <br/><br/> <strong>Topic:</strong> {2}, <br/><br/>  <strong>Description:</strong> {3}. <br/><br/>

user_updated_ticket_subject = [PRIME] Ticket status updated
user_updated_ticket_button_title = Check updated ticket
user_updated_ticket_cover_title = Ticket status <br> updated
user_updated_ticket_paragraph = The status of your ticket <strong>{0}</strong>, has changed from <br/><strong>{1}</strong> to <strong>{2}</strong>. <br/><br/>

user_added_comment_subject =  [PRIME] A comment has been added to a ticket
user_added_comment_cover_title = A comment has been <br> added to a ticket
user_added_comment_paragraph = User <strong>{0}</strong> has created a comment at a ticket: <strong>{1}</strong> <br/> with the following description:<br/> <strong>{2}</strong> <br/><br/>
user_added_comment_button_title = Check ticket
#</Ticket related emails>

#<Tasks related emails>
TODO="To Do"
IN_PROGRESS= "In Progress"
IN_REVIEW = "In Review"
DONE = "Done"
ARCHIVED = "Archived"

user_updated_task_button_title = Check task

user_assignee_task_subject = [PRIME] {0} has been assigned to you
user_assignee_task_cover_title = Somebody assigned <br> a task to you

user_assignee_updated_task_subject = [PRIME] {0} you are assigned has been updated
user_assignee__updated_task_cover_title = Somebody updated <br> your task

user_added_task_subject = [PRIME] {0} has been created
user_added_task_cover_title = A Task has <br> been created

user_added_task_paragraph = User <strong>{0}</strong> has added a task for you with the following description:<br>Title: <strong>{1}</strong> and task status: <strong>{2}</strong> and description: <strong>{3}</strong> <br/><br/>
user_added_task_paragraph_no_des = User <strong>{0}</strong> has added a task for you with the following description:<br>Title: <strong>{1}</strong> and task status: <strong>{2}</strong>

user_updated_task_subject = [PRIME] {0} has been updated
user_updated_task_cover_title =  Task has been <br> updated
user_updated_task_paragraph = User <strong>{0}</strong> has updated the task with the following description:<br>Title: <strong>{1}</strong> and task status: <strong>{2}</strong> and description: <strong>{3}</strong> <br/><br/>
user_updated_task_paragraph_no_des = User <strong>{0}</strong> has updated the task with the following description:<br>Title: <strong>{1}</strong> and task status: <strong>{2}</strong>

user_deleted_assignee_task_subject = [PRIME] {0} you are assigned has been removed
user_deleted_assignee_task_cover_title = Task you are assigned <br> has been removed

user_deleted_task_subject = [PRIME] {0} has been removed
user_deleted_task_cover_title =  Task has been <br> removed
user_deleted_task_paragraph = User <strong>{0}</strong> has deleted the task with the following description:<br>Title: <strong>{1}</strong> and task status: <strong>{2}</strong> and description: <strong>{3}</strong> <br/><br/>
user_deleted_task_paragraph_no_des = User <strong>{0}</strong> has deleted the task with the following description:<br>Title: <strong>{1}</strong> and task status: <strong>{2}</strong>

user_added_task_comment_subject =  [PRIME] {0} commented on {1}
user_added_task_comment_cover_title = Somebody commented <br> on your task
user_added_task_comment_paragraph = User <strong>{0}</strong> has added a comment at a task with the following description:<br>Title: <strong>{1}</strong> and description: <strong>{2}</strong> <br/><br/>
user_added_task_comment_button_title = Check comment
#</Tasks related emails>

friendly_reminder_subject = Friendly reminder
friendly_reminder_title  = Friendly reminder
friendly_reminder_user_title = Hi {0}
friendly_reminder_button_title = Go to PRIME
friendly_reminder_paragraph1 = We have notice that you have not use PRIME for a while. <br/> We just wanted to check in with you and make sure that everything is clear. if you have any question, than please get in touch with your system administrator.
friendly_reminder_paragraph2 = <br/>Cheers,<br/>The team at PRIME.


footer_message = This is an automatically generated email, please do not reply to it. If you have any questions, then please contact your system administrator.
user_title = Hi {0},
second_paragraph = <br/>Cheers,<br/>The team at PRIME

user_access_prime_subject = [PRIME] You have been given access to PRIME. Here is your account confirmation.
user_access_prime_body = Good news! Your system administrator has given you access to PRIME. The intuitive data and analytics platform for all business professionals. <br/> Please contact your system administrator for your account details. <br/><br/> Once you are logged in, you can visit your <strong>Account page</strong> if you want to change your password or set your preferences. 
user_access_button_title = Log in
user_access_cover_title =  Welcome to <br/> PRIME



task_assign_subject = [PRIME] User {0} has assigned a task to you.
task_assign_body = You have a new task from the project <strong>{0}</strong>. <br/> User <strong>{1}</strong> has assigned you the task: <strong>{2}</strong>. <br/><br/> Click on the button to see the task.
task_assign_cover_title =  New task <br/> assigned to you
task_assign_button_title =  See task

user_locked_out_subject = [PRIME] Account temporary blocked
user_locked_out_body = Please note that your account has been temporarily blocked due to many failed login attempts. <br/> You will be able to login to PRIME again after <strong>{0}</strong>. We do this to ensure your safety. <br/>   If you have any question, then please contact the system administrator.
user_locked_out_cover_title = Account <br/> temporary blocked



mdp_new_entries_subject = [PRIME] User {0} has uploaded new data on the {1} data source
mdp_new_entries_button_name = Investigate it
mdp_new_entries_firstParagraph = User <strong>{0}</strong> has manually uploaded new data at the data source <strong>{1}</strong>.
mdp_new_entries_cover_title = New Data <br/> Upload


mdp_changes_approved_subject =[PRIME] Data upload approved
mdp_changes_approved_body = Your data upload at topic <strong>{1}</strong> has been approved by <strong>{0}</strong>.<br/>Click the button to go to the topic.
mdp_changes_approved_body_with_note = Your data upload at topic <strong>{1}</strong> has been approved by <strong>{0}</strong>.<br/><strong>{0}</strong> left the following message: <strong>{2}</strong>
mdp_changes_approved_cover_title =  Data upload <br/> approved
mdp_changes_approved_button_title = Go to the topic

mdp_changes_submit_subject =[PRIME] New Data has been submitted for approval
mdp_changes_submit_body = A new data upload at topic <strong>{1}</strong> by <strong>{0}</strong> is awaiting your approval.<br/>Click the button to go to the topic.
mdp_changes_submit_body_with_note = A new data upload at topic <strong>{1}</strong> by <strong>{0}</strong> is awaiting your approval.<br/><strong>{0}</strong> left the following message: <strong>{2}</strong>
mdp_changes_submit_cover_title =  Data upload <br/> awaiting approval
mdp_changes_submit_button_title = Go to the topic

mdp_changes_declined_subject =[PRIME] Data upload declined
mdp_changes_declined_cover_title = Data upload <br/> is declined 
mdp_changes_declined_buttonName = Go to the topic
mdp_changes_declined_firstParagraph = Your data at topic <strong>{0}</strong> has been declined by <strong>{1}</strong>
mdp_changes_declined_firstParagraph_with_note = Your data at topic <strong>{0}</strong> has been declined by <strong>{1}</strong><br/><strong>{1}</strong> left the following message: <strong>{2}</strong>

mdp_editor_reminder_subject = [PRIME] Reminder about new batch of data
mdp_editor_reminder_cover_title = Reminder about new <br/> batch of data
mdp_editor_reminder_body = We wanted to remind you to upload a new batch of data at the topic: <strong>{0}</strong>.<br/>Click on the button to upload the new data.
mdp_editor_reminder_button_title = Upload new data


mdp_auditor_reminder_subject = [PRIME] Reminder about new batch of data
mdp_auditor_reminder_body = We wanted to remind you that new batch of data will soon be uploaded at the topic: <strong>{0}</strong>.<br/>Click on the button to see and approve/decline the new data.
mdp_auditor_reminder_cover_title = Reminder about new <br/> batch of data
mdp_auditor_reminder_button_title = Go to the data



mdp_viewer_reminder_subject = [PRIME] Reminder about new batch of data
mdp_viewer_reminder_body =  We wanted to remind you that new batch of data will soon be uploaded at the topic: <strong>{0}</strong>.<br/>Follow this link to see the new data: </br> <strong>{1}</strong>.
mdp_viewer_reminder_cover_title = Reminder about <br/> new batch of data


# VETL Strings
vetl_pipeline_completed_subject = [PRIME] Data Pipeline Completed
vetl_pipeline_completed_body =  The data pipeline <strong>{0}</strong>, <strong>{1}</strong>, that was scheduled to run <strong>{2}</strong> has completed successfully.
vetl_pipeline_completed_cover_title = Data pipeline <br/> completed
vetl_pipeline_completed_button_title = Check data pipeline

vetl_pipeline_failed_subject = [PRIME] Data Hub Pipeline Failed
vetl_pipeline_failed_body = The pipeline <b>{0}, {1}</b>, scheduled to run <b>{2}</b> has failed executing. <br/>Click the button to go to the pipeline, so you can check the logs.
vetl_pipeline_failed_cover_title = Data Hub <br/> Pipeline Failed
vetl_pipeline_failed_button_title = Go to the pipeline

vetl_pipeline_long_running_subject = [PRIME] Data Hub Pipeline Running Too Long
vetl_pipeline_long_running_body = The pipeline <b>{0}, {1}</b>, scheduled to run <b>{2}</b> that you have scheduled has been running for too long. Since this running time exceeds our limit for direct processing during peak hours, we have postponed the schedule.<br/><br/>A PRIME employee will manually reschedule the pipeline for you as soon as possible (during off-peak hours), so that it can finish without any issues. We will notify you once your pipeline has been rescheduled and completed.<br/><br/> Click the button to go to the pipeline, so you can check the logs <br/><br/>The fair-use™ policy on data processing helps us to ensure platform stability for all users. We trust that you will benefit from this way of working. Thanks for your flexibility.
vetl_pipeline_long_running_cover_title =  Data Hub Pipeline <br/> Running Too Long
vetl_pipeline_long_running_button_title = Go to the pipeline

vetl_pipeline_cancelled_subject = [PRIME] Data Hub Pipeline Cancelled
vetl_pipeline_cancelled_body = The pipeline <b>{0}, {1}</b>, scheduled to run <b>{2}</b> has been cancelled.<br/><br/>Click on the button to go to the pipeline, so you can check the logs.
vetl_pipeline_cancelled_button_title = Go to the pipeline
vetl_pipeline_cancelled_cover_title = Data Hub Pipeline <br/> Cancelled

vetl_parsing_problem = Unable to parse the configuration! Please remove invalid elements and try again!

# hibernate errors
field_not_empty = Field {0} is required

package io.training.api.actions;

import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;

import java.util.concurrent.CompletionStage;

/**
 * Created by Agon on 09/08/2020
 */
public class AuthenticatedAction extends Action<Authenticated>  {

	@Override
    public CompletionStage<Result> call(Http.Request request) {
		return delegate.call(request);
    }
}

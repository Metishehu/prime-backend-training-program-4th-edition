package io.training.api.models.examples;

/**
 * Created by agonlohaj on 29 Sep, 2020
 */
public enum Membership {
	NONE,
	FREE,
	SUBSCRIBER,
	PREMIUM
}

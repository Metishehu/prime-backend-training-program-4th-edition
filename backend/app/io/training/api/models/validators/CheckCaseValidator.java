package io.training.api.models.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by agonlohaj on 31 Aug, 2020
 */
public class CheckCaseValidator implements ConstraintValidator<CheckCase, String> {

	private CaseMode caseMode;
	private String message;

	public void initialize (CheckCase constraintAnnotation) {
		this.caseMode = constraintAnnotation.value();
		this.message = constraintAnnotation.message();
	}

	public boolean isValid (String object, ConstraintValidatorContext constraintContext) {
		if (object == null)
			return true;

		boolean isValid;
		if (caseMode == CaseMode.UPPER) {
			isValid = object.equals(object.toUpperCase());
		} else {
			isValid = object.equals(object.toLowerCase());
		}

		if (!isValid) {
			constraintContext.disableDefaultConstraintViolation();
			constraintContext.buildConstraintViolationWithTemplate(message).addConstraintViolation();
		}
		return isValid;
	}

}

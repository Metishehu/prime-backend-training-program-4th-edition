package io.training.api.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import io.training.api.models.Taxi;
import io.training.api.mongo.IMongoDB;
import org.bson.types.ObjectId;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TaxiPojoMongoController extends Controller {

	@Inject
	IMongoDB mongoDB;

	public Result all (Http.Request request) {
		return ok(Json.toJson(new ArrayList<>()));
	}

	@BodyParser.Of(BodyParser.Json.class)
	public Result update (Http.Request request, String id) {
		return ok(Json.newObject());
	}

	@BodyParser.Of(BodyParser.Json.class)
	public Result delete (Http.Request request, String id) {
		return ok(Json.newObject());
	}

	@BodyParser.Of(BodyParser.Json.class)
	public Result save (Http.Request request) {
		JsonNode node = request.body().asJson();
		Taxi taxi = Json.fromJson(node, Taxi.class);
		mongoDB.getMongoDatabase()
			.getCollection("pojo", Taxi.class)
			.insertOne(taxi);

		return ok(Json.toJson(taxi));
	}

}
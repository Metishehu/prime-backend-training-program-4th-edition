package io.training.api.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.training.api.models.User;
import play.Logger;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

public class TaxiController extends Controller {


	public Result all (Http.Request request) {
		try {
			ObjectNode node = Json.newObject();
			node.put("emri", "Agon");
			node.put("mbiemri", "asfgsg");
			node.put("age", "asfsg");

			User usr = Json.fromJson(node, User.class);
			return ok(Json.toJson(usr));
		} catch (Exception ex) {
			ex.printStackTrace();
			return badRequest("oo no no");
		}
	}

	public Result saveById (Http.Request request, String id) {
		JsonNode node = request.body().asJson();
		if (node.isArray()) {
			return badRequest("expected object not array");
		}

		return ok(node);
	}

	@BodyParser.Of(BodyParser.Json.class)
	public Result delete (Http.Request request, String id) {
		JsonNode node = request.body().asJson();

		if (!node.isObject()) {
			return badRequest("expected object not array");
		}
		ObjectNode object = (ObjectNode) node;

		JsonNode name = object.get("name");


		return ok(node);
	}

	@BodyParser.Of(BodyParser.Json.class)
	public Result save (Http.Request request) {
		JsonNode node = request.body().asJson();
		if (node.isArray()) {
			return badRequest("not expecting an array");
		}
		try {
			User user = Json.fromJson(node, User.class);

			return ok(Json.toJson(user));
		} catch (Exception ex) {
			ex.printStackTrace();
			ObjectNode response = Json.newObject();
			response.put("message", "Invalid object supplied, I expect the user to have age parameter set correctly");
			return badRequest(response);
		}
	}

	@BodyParser.Of(BodyParser.Json.class)
	public Result update (Http.Request request) {
		JsonNode node = request.body().asJson();

		return ok(node);
	}

}
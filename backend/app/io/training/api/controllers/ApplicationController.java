package io.training.api.controllers;
import akka.stream.IOResult;
import akka.stream.javadsl.FileIO;
import akka.stream.javadsl.Source;
import akka.util.ByteString;
import com.google.inject.Inject;
import com.typesafe.config.Config;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import play.http.HttpEntity;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.*;

import java.util.Collections;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

@Api(value = "Application Controller")
/**
 * The controller that handles all home related routing
 */
public class ApplicationController extends Controller {
	private final HttpExecutionContext ec;
	@Inject
	Config config;

	@Inject
	public ApplicationController (HttpExecutionContext ec) {
		this.ec = ec;
	}

	@ApiOperation(value = "Health Check 3242")
	public CompletableFuture<Result> healthCheck() {
		return CompletableFuture.supplyAsync(Results::ok, ec.current());
	}

	@ApiOperation(value = "Swagger Documentation")
	public Result swagger (Http.Request request) {
		return ok(views.html.swagger.render(config));
	}

	/**
	 * Show the home page
	 * @param page - the page to show
	 * @return
	 */
	public Result show(String page) {
		java.io.File file = new java.io.File("/tmp/fileToServe.pdf");
		java.nio.file.Path path = file.toPath();
		Source<ByteString, CompletionStage<IOResult>> source = FileIO.fromPath(path);
		return new Result(
			new ResponseHeader(200, Collections.emptyMap()),
			new HttpEntity.Streamed(source, Optional.empty(), Optional.of("text/plain"))
		);
	}
}
package io.training.api.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import io.training.api.utils.DatabaseUtils;
import org.bson.Document;
import org.bson.types.ObjectId;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

import java.util.ArrayList;
import java.util.List;

public class TaxiRawMongoController extends Controller {


	public Result all (Http.Request request) {
		MongoClient mongo = MongoClients.create();
		MongoDatabase database = mongo.getDatabase("training");
		MongoCollection<Document> collection = database.getCollection("raw");

		List<Document> taxis = collection.find().into(new ArrayList<>());

		// Close the connection
		mongo.close();
		return ok(Json.toJson(taxis));
	}

	@BodyParser.Of(BodyParser.Json.class)
	public Result save (Http.Request request) {
		JsonNode node = request.body().asJson();
		if (!node.isObject()) {
			return badRequest(Json.toJson("ooo no no"));
		}
		ObjectNode object = (ObjectNode) node;
		Document document = DatabaseUtils.toDocument(object);

		MongoClient mongo = MongoClients.create();
		MongoDatabase database = mongo.getDatabase("training");
		MongoCollection<Document> collection = database.getCollection("raw");

		collection.insertOne(document);

		mongo.close();
		return ok(object);
	}

	@BodyParser.Of(BodyParser.Json.class)
	public Result update (Http.Request request, String id) {
		JsonNode node = request.body().asJson();
		if (!node.isObject()) {
			return badRequest(Json.toJson("ooo no no"));
		}
		ObjectNode object = (ObjectNode) node;
		Document document = DatabaseUtils.toDocument(object);

		MongoClient mongo = MongoClients.create();
		MongoDatabase database = mongo.getDatabase("training");
		MongoCollection<Document> collection = database.getCollection("raw");

		collection.replaceOne(Filters.eq("_id", new ObjectId(id)), document);

		mongo.close();
		return ok(object);
	}

	@BodyParser.Of(BodyParser.Json.class)
	public Result delete (Http.Request request, String id) {
		JsonNode node = request.body().asJson();
		if (!node.isObject()) {
			return badRequest(Json.toJson("ooo no no"));
		}
		MongoClient mongo = MongoClients.create();
		MongoDatabase database = mongo.getDatabase("training");
		MongoCollection<Document> collection = database.getCollection("raw");

		collection.deleteOne(Filters.eq("_id", new ObjectId(id)));

		mongo.close();
		return ok(node);
	}

}
package io.training.api.controllers;

import com.google.inject.Inject;
import io.training.api.models.requests.BinarySearchRequest;
import io.training.api.models.requests.NameValuePair;
import io.training.api.services.AssignemntTwoService;
import io.training.api.services.SerializationService;
import io.training.api.utils.DatabaseUtils;
import play.mvc.*;

import java.util.concurrent.CompletableFuture;

public class AssignmentLectureTwoController extends Controller {

	@Inject
	SerializationService serializationService;

	@Inject
	AssignemntTwoService service;

	@BodyParser.Of(BodyParser.Json.class)
	public CompletableFuture<Result> function1 (Http.Request request) {
		return serializationService.parseListBodyOfType(request, Integer.class)
				.thenCompose((data) -> service.function1(data))
				.thenCompose((data) -> serializationService.toJsonNode(data))
				.thenApply(Results::ok)
				.exceptionally(DatabaseUtils::throwableToResult);
	}

	@BodyParser.Of(BodyParser.Json.class)
	public CompletableFuture<Result> function2 (Http.Request request) {
		return serializationService.parseListBodyOfType(request, Integer.class)
				.thenCompose((data) -> service.function2(data))
				.thenCompose((data) -> serializationService.toJsonNode(data))
				.thenApply(Results::ok)
				.exceptionally(DatabaseUtils::throwableToResult);
	}

	@BodyParser.Of(BodyParser.Json.class)
	public CompletableFuture<Result> function3 (Http.Request request) {
		return serializationService.parseListBodyOfType(request, Integer.class)
				.thenCompose((data) -> service.function3(data))
				.thenCompose((data) -> serializationService.toJsonNode(data))
				.thenApply(Results::ok)
				.exceptionally(DatabaseUtils::throwableToResult);
	}

	@BodyParser.Of(BodyParser.Json.class)
	public CompletableFuture<Result> function4 (Http.Request request) {
		return serializationService.parseListBodyOfType(request, Integer.class)
				.thenCompose((data) -> service.function4(data))
				.thenCompose((data) -> serializationService.toJsonNode(data))
				.thenApply(Results::ok)
				.exceptionally(DatabaseUtils::throwableToResult);
	}

	@BodyParser.Of(BodyParser.Json.class)
	public CompletableFuture<Result> function5 (Http.Request request) {
		return serializationService.parseListBodyOfType(request, Integer.class)
				.thenCompose((data) -> service.function5(data))
				.thenCompose((data) -> serializationService.toJsonNode(data))
				.thenApply(Results::ok)
				.exceptionally(DatabaseUtils::throwableToResult);
	}

	@BodyParser.Of(BodyParser.Json.class)
	public CompletableFuture<Result> function6 (Http.Request request) {
		return serializationService.parseListBodyOfType(request, Integer.class)
				.thenCompose((data) -> service.function6(data))
				.thenCompose((data) -> serializationService.toJsonNode(data))
				.thenApply(Results::ok)
				.exceptionally(DatabaseUtils::throwableToResult);
	}

	@BodyParser.Of(BodyParser.Json.class)
	public CompletableFuture<Result> function7 (Http.Request request) {
		return serializationService.parseListBodyOfType(request, NameValuePair.class)
				.thenCompose((data) -> service.function7(data))
				.thenCompose((data) -> serializationService.toJsonNode(data))
				.thenApply(Results::ok)
				.exceptionally(DatabaseUtils::throwableToResult);
	}

	@BodyParser.Of(BodyParser.Json.class)
	public CompletableFuture<Result> function8 (Http.Request request) {
		return serializationService.parseListBodyOfType(request, NameValuePair.class)
				.thenCompose((data) -> service.function8(data))
				.thenCompose((data) -> serializationService.toJsonNode(data))
				.thenApply(Results::ok)
				.exceptionally(DatabaseUtils::throwableToResult);
	}

	@BodyParser.Of(BodyParser.Json.class)
	public CompletableFuture<Result> function9 (Http.Request request) {
		return serializationService.parseListBodyOfType(request, NameValuePair.class)
				.thenCompose((data) -> service.function9(data))
				.thenCompose((data) -> serializationService.toJsonNode(data))
				.thenApply(Results::ok)
				.exceptionally(DatabaseUtils::throwableToResult);
	}

	@BodyParser.Of(BodyParser.Json.class)
	public CompletableFuture<Result> binarySearch (Http.Request request) {
		return serializationService.parseBodyOfType(request, BinarySearchRequest.class)
				.thenCompose((data) -> service.binarySearch(data))
				.thenCompose((data) -> serializationService.toJsonNode(data))
				.thenApply(Results::ok)
				.exceptionally(DatabaseUtils::throwableToResult);
	}
}
package io.training.api.controllers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import io.training.api.services.SerializationService;
import io.training.api.utils.DatabaseUtils;
import play.libs.Files;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class LectureFiveController extends Controller {

	@Inject
	ObjectMapper mapper;

	@Inject
	SerializationService serializationService;

	@BodyParser.Of(BodyParser.MultipartFormData.class)
	public Result upload (Http.Request request) throws IOException {
		Http.MultipartFormData<File> body = request.body().asMultipartFormData();
		List<Http.MultipartFormData.FilePart<File>> files = body.getFiles();
		if (files.size() == 0) {
			return badRequest("Missing files");
		}
		Http.MultipartFormData.FilePart<File> file = files.get(0);
		Files.TemporaryFile raw = (Files.TemporaryFile) file.getRef();
		String fileName = file.getFilename();
		ObjectNode node = Json.newObject();
		node.put("name", fileName);
		return ok(node);
	}

	@BodyParser.Of(BodyParser.MultipartFormData.class)
	public Result uploadJson (Http.Request request) throws IOException {
		Http.MultipartFormData<File> body = request.body().asMultipartFormData();
		List<Http.MultipartFormData.FilePart<File>> files = body.getFiles();
		if (files.size() == 0) {
			return badRequest("Missing files");
		}
		Http.MultipartFormData.FilePart<File> file = files.get(0);
		File raw = file.getFile();
		JsonNode node = serializationService.fileToObjectNode(raw);
		return ok(node);
	}
}
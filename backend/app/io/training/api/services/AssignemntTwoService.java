package io.training.api.services;
import java.util.Map; 
import java.util.stream.IntStream;
import com.google.inject.Inject;
import io.training.api.models.requests.BinarySearchRequest;
import io.training.api.models.requests.NameValuePair;
import io.training.api.models.responses.ChartData;
import play.Logger;
import play.libs.concurrent.HttpExecutionContext;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import java.util.Comparator;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.stream.Collectors;
/**
 * Created by Agon on 09/08/2020
 */
@Singleton
public class AssignemntTwoService {
    @Inject
    HttpExecutionContext ec;


    /**
     * Function as Line: y = x * 2
     * @param input
     * @return
     */
    public CompletableFuture<List<Integer>> function1 (List<Integer> input) {
        return CompletableFuture.supplyAsync(() -> {
            return input.stream().map(x -> x * 2).collect(Collectors.toList());
            
        }, ec.current());
    }


    /**
     * Function as Scatter y = square root of the absolute value of ((x ^ 2) + (x * 4))
     * @param input
     * @return
     */
    public CompletableFuture<List<Double>> function2 (List<Integer> input) {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {
            //System.out.println(input.stream().map(x->((x*x)+(x*4))).collect(Collectors.toList()) ); 
            //
            //System.out.println(input.stream().map(y->Math.sqrt(Math.abs((y*y)+(y*4)  ) )   ).collect(Collectors.toList())+" this is one ");
            return input.stream().map(x->Math.sqrt(Math.abs((x*x)+(x*4) ))).collect(Collectors.toList()); 

            // TODO: do something smarter here.
        }, ec.current());
    }

    /**
     * Function y = If 3^2 - x^2 > 0 than square root of (3^2 - x^2). If 3^2 - x^2 < 0 then - square root of absolute value of (3^2 - x^2)
     * @param input
     * @return
     */
    public CompletableFuture<List<Double>> function3 (List<Integer> input) {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {
            return input.stream()
                .map(x->{
                    double value = (9-(x*x));
                    if(value >0){
                        return Math.sqrt(value);
                    }else{
                        return Math.sqrt(Math.abs(value))*-1;
                    }
                } ).collect(Collectors.toList()); 

        }, ec.current());
    }

    /**
     * Function as Line: y = sin(x)
     * @param input
     * @return
     */
    public CompletableFuture<List<Double>> function4 (List<Integer> input) {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {
            return input.stream().map(x-> Math.sin(x)).collect(Collectors.toList());
        }, ec.current());
    }

    /**
     * Function as Line: y = cos(x)
     * @param input
     * @return
     */
    public CompletableFuture<List<Double>> function5 (List<Integer> input) {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {
            return input.stream().map(x-> Math.cos(x)).collect(Collectors.toList());
        }, ec.current());
    }

    /**
     * 2 Line Functions displayed together, one for Sin and one for Cos, check only the series option to include two of them
     * Returns two lists of double the first list for sin, and the second one for cos
     * @param input
     * @return
     */
    public CompletableFuture<List<List<Double>>> function6 (List<Integer> input) {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {
            List<List<Double>> result = new ArrayList<>();
            result.add(input.stream().map(x-> Math.sin(x)).collect(Collectors.toList()));
            result.add(input.stream().map(x-> Math.cos(x)).collect(Collectors.toList()));
            return result;
        }, ec.current());
    }
    
    /**
     * I want to see the top 4 performing words, given the randomCategoryData, the top 4 with the highest random generated value
     * Make sure the values are summed on repeated words (no duplicates)
     * @param input
     * @return
     */
    public CompletableFuture<List<NameValuePair>> function7 (List<NameValuePair> input) {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {
            try {
                Map<String, Integer> aggregatedValues = input.stream()
                    .collect(Collectors.groupingBy(NameValuePair::getName, Collectors.summingInt(NameValuePair::getValue)));


                List<NameValuePair> og = aggregatedValues.entrySet().stream()
                    .map(entry -> new NameValuePair(entry.getKey(),entry.getValue()))
                    .collect(Collectors.toList());
                List<NameValuePair> a = og.stream()
                    .sorted(Comparator.comparingInt(NameValuePair::getValue).reversed())
                    .limit(4)
                    .collect(Collectors.toList());

                
                return a;
            } catch (Exception ex) {
                ex.printStackTrace();
                throw new CompletionException(ex);
            }
        }, ec.current());
    }

    /**
     * Calculate the average within the groups now, and show that here. Check the random Category data on how it generates those
     * @param input
     * @return
     */
    public CompletableFuture<List<ChartData>> function8 (List<NameValuePair> input) {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {
            
        Map<String, Double> averageMap = input.stream()
            .collect(Collectors.groupingBy(
                NameValuePair::getName,
                Collectors.averagingInt(NameValuePair::getValue)
            ));
            

           // List<ChartData> top4WordValues = averageMap.entrySet().stream()
             //   .map(entry -> new ChartData(entry.getKey(), entry.getValue()))
               // .collect(Collectors.toList());
            List<ChartData> output=  averageMap.entrySet().stream()
                .map(entry-> new ChartData(entry.getKey(),entry.getValue()))
                .collect(Collectors.toList());

                return output; 
        }, ec.current());
    }

    /**
     * Calculate the values such that they are cumulative, each subsequent is summed with the total so far!
     * @param input
     * @return
     */
    public CompletableFuture<List<NameValuePair>> function9 (List<NameValuePair> input) {
        // TODO: Implement this functio   

        return CompletableFuture.supplyAsync(() -> {
            Map<String,Integer>reducing =  input.stream().collect(Collectors.groupingBy(NameValuePair::getName, Collectors.reducing(0,NameValuePair::getValue,Integer::sum)));
            
            List<NameValuePair> res = reducing.entrySet().stream()
                .map(x-> new NameValuePair(x.getKey(),x.getValue()))
                .collect(Collectors.toList());
            

            return res; 
        }, ec.current());
    }

    /**
     * Calculate the values such that they are cumulative, each subsequent is summed with the total so far!
     * @param requestjjjjj
     * @return index - the index of the search
     */
    public Integer binaryS (List<Integer> L, Integer Target){
         // shoud of made it another file then import it here but it was esay to do this faster so yea 
        Integer mid = L.size()/2;
        Integer value = L.get(mid);

        List<Integer> Left = IntStream.range(0,mid).mapToObj(L::get).collect(Collectors.toList());
        List<Integer> Right = IntStream.range(mid,L.size()).mapToObj(L::get).collect(Collectors.toList());
        //System.out.println(Left+"--"+Right+" T"+Target);
        if(value==Target){
            return mid;
        }
        if(L.size()<=1){return -1;}
        if(value<Target){
            //System.out.println("Lower"+" Val"+value+" "+Target);
            return mid+binaryS(Right,Target);
        }
        if (value>Target){
            //System.out.println("Heigher"+" Val"+value+" "+Target );
            return binaryS(Left,Target);
        }

        
        return -1;
    }
    public CompletableFuture<Integer> binarySearch (BinarySearchRequest request) {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {
            Integer a = binaryS(request.getValues(),request.getSearch());
            return a;
        }, ec.current());
    }
}


